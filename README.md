# TYPESCRIPT ASSIGNMENTS

## ASSIGNMENT 1

Calculate the distance between 2 coordinate points.

## ASSIGNMENT 2

Create interface for X coordinates and Y coordinates and then find the distance between the two points.

## ASSIGNMENT 3

Find what kind of triangle is formed when 3 coordinate points is given. It can be equilateral, isosceles or scalene triangle.

## ASSIGNMENT 4

Convert the string points to the Array of the X and Y coordinate. Then find the distance between the 2 points.

## ASSIGNMENT 5

Create a class with attributes getItem, setItem, deleteItem and displayItem

## ASSIGNMENT 6

Create a class shape with attributes noOfSides, points, polygonName and method findShape

## ASSIGNMENT 7

Create a class shape as base class and sub classes line, triangle and quadrilateral that inherits all the properties of its parent class shape.

## ASSIGNMENT 8

Create a class shape as base class and sub classes line, triangle and quadrilateral that inherits all the properties of its parent class shape with getter ans setter and find the area of the shapes.

## ASSIGNMENT 9

Create a files main, decorator, calculation and interface with different functionalities and use export and import module to access one file in another.


## INSTALL TYPESCRIPT

--- 
to run type script we need node and npm package to be installed.
npm install -g typescript
---

## TO RUN THE TYPESCRIPT FILE IN THE COMMAND PROMT
---
tsc filename.ts
node filename.js
---